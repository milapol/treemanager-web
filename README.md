# treemanager-web

Webapp for treemanager-api

# Screenshots
| ![treemanager landing page](screenshots/logged_in.png) | ![treemanager sidebar](screenshots/sidebar.png) | ![treemanager add form](screenshots/form.png)
|---|---|---|